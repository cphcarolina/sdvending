
import java.net.*;

/**
 * ServidorMiniHTTP: Servidor MiniHTTP y controlador que 
 de las peticiones y de lanzar el HiloControlador.
 * @author Carolina Prada Hernández
 */
public class ServidorMiniHTTP {

    public static void main(String[] args) {
        String puerto = "9998";
        String fichero = "maquinas.txt";

        try {

            if (args.length!=1) {
                System.out.println("Debe indicar el puerto de escucha del servidor.");
                System.out.println("$./servidor puerto_servidor");
                System.exit(1);
            }

            puerto = args[0];
            
            ServerSocket skServidor = new ServerSocket(Integer.parseInt(puerto));
            System.out.println("Escuchando el puerto " + puerto);

            Thread controlador = new HiloControlador(fichero);
            controlador.start();
            
            for (;;) {
                try {
                    Socket skCliente = skServidor.accept();
                    System.out.println("Sirviendo a cliente...");

                    Thread thread = new HiloMiniHTTP(skCliente, fichero);
                    thread.start();
                } catch (Exception e) {
                    System.out.println("Error MiniHTTP: " + e.getMessage());
                    e.printStackTrace();
                    System.exit(1);
                }
            }
        } catch (Exception e) {
            System.out.println("Error MiniHTTP: " + e.getMessage());
            e.printStackTrace();
            System.exit(1);
        }
    }
}
