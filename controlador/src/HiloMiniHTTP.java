import java.io.*;
import java.rmi.*;
import java.net.Socket;

/**
 * HiliMiniHTTP: hilo que lanza el servidor MiniHTTP para gestionar cada
 * consulta y de la comunicación RMI con el servidor.
 *
 * @author Carolina Prada Hernández
 */
public class HiloMiniHTTP extends Thread {

    /**
     * Socket recibido
     */
    private final Socket skCliente;

    /**
     * Nombre del fichero con los datos de los objetos remotos
     */
    private final String fichero;

    /**
     * La cantidad de datos que se almacenan por cada objeto remoto
     */
    private final int total_datos = 8;

    /**
     * Matriz para almacenar los datos leídos en el fichero
     */
    private String[][] maquinas;

    /**
     * Contructor de HiloMiniHTTP
     *
     * @param p_sk el socket recibido
     * @param p_fichero nombre del fichero donde se encuentran los datos de los
     * objetos remotos
     */
    public HiloMiniHTTP(Socket p_sk, String p_fichero) {
        skCliente = p_sk;
        fichero = p_fichero;

        maquinas = null;
    }

    //<editor-fold defaultstate="collapsed" desc=" Comunicación Sockets ">
    /**
     * Obtiene las peticiones por parte del Gestor
     *
     * @param p_sk el socket recibido
     * @return la petición del socket
     */
    private String leerSocket(Socket p_sk) {
        String datos = "";

        try {
            InputStream is = p_sk.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            datos = br.readLine();
        } catch (Exception e) {
            System.out.println("Error HiloMiniHTTP (leerSocket): " + e.getMessage());
            e.printStackTrace();
        }

        return datos;
    }

    /**
     * Devuelve el html que se le muestra al Gestor
     *
     * @param p_sk socket que enviar
     * @param p_html respuesta a la petición
     */
    private void escribirSocket(Socket p_sk, String p_html) {
        try {
            OutputStream os = p_sk.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);
            dos.writeUTF(p_html);
        } catch (Exception e) {
            System.out.println("Error HiloMiniHTTP (escribirSocket): " + e.getMessage());
            e.printStackTrace();
        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="colapsed" desc=" Comunicación RMI ">
    /**
     * Modifica la temperatura de la máquina remota indicada
     *
     * @param p_id identificador de la máquina remota
     * @param p_valor nueva temperatura
     * @return cuerpo de la respuesta
     */
    private String gestionarTemperatura(int p_id, int p_valor) {
        InterfazMaquina maquina = null;
        String resultado = "", url = "";

        try {
            url += maquinas[p_id][0];

            System.setSecurityManager(new RMISecurityManager());
            maquina = (InterfazMaquina) Naming.lookup(url);
            maquina.setTemperatura(p_valor);
            leerMaquinas();
            resultado = mostrarMaquina(p_id);

        } catch (Exception e) {
            System.out.println("Error HiloMiniHTTP (refrigerar): " + e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    /**
     * Modifica el stock de la máquina remota indicada
     *
     * @param p_id identificador de la máquina remota
     * @param p_valor nuevo stock
     * @return cuerpo de la respuesta
     */
    private String gestionarStock(int p_id, int p_valor) {
        InterfazMaquina maquina = null;
        String url = "";
        String resultado = "";

        try {
            url += maquinas[p_id][0];

            System.setSecurityManager(new RMISecurityManager());
            maquina = (InterfazMaquina) Naming.lookup(url);
            maquina.setStock(p_valor);
            leerMaquinas();
            resultado = mostrarMaquina(p_id);
        } catch (Exception e) {
            System.out.println("Error HiloMiniHTTP (gestionarStock): " + e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    /**
     * Modifica el monedero de la máquina remota indicada
     *
     * @param p_id identificador de la máquina remota
     * @param p_valor nuevo valor del monedero
     * @return cuerpo de la respuesta
     */
    private String gestionarMonedero(int p_id, int p_valor) {
        InterfazMaquina maquina = null;
        String url = "";
        String resultado = "";

        try {
            url += maquinas[p_id][0];

            System.setSecurityManager(new RMISecurityManager());
            maquina = (InterfazMaquina) Naming.lookup(url);
            maquina.setMonedero(p_valor);
            leerMaquinas();
            resultado += mostrarMaquina(p_id);
        } catch (Exception e) {
            System.out.println("Error HiloMiniHTTP (gestionarMonedero): " + e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    /**
     * Cambia el estado de la máquina, si estaba encendida la apaga y viceversa
     *
     * @param p_pet máquina a modificar ("maquina=<id>")
     * @return cuerpo de la respuesta
     */
    private String interruptor(String p_pet) {
        InterfazMaquina maquina = null;
        String url = "";
        String resultado = "";
        String parametro;
        int id;

        try {
            parametro = getParam(p_pet);

            if (parametro.equals("maquina")) {
                id = getValorInt(p_pet);
                url += maquinas[id][0];

                System.setSecurityManager(new RMISecurityManager());
                maquina = (InterfazMaquina) Naming.lookup(url);
                if (maquinas[id][6].equals("true")) {
                    maquina.setEstado(false);
                    maquinas[id][6] = "false";
                } else {
                    maquina.setEstado(true);
                    maquinas[id][6] = "true";
                }
                resultado = mostrarMaquina(id);
            } else {
                resultado = "405";
            }
        } catch (Exception e) {
            System.out.println("Error HiloMiniHTTP (interruptor): " + e.getMessage());
            e.printStackTrace();
        }

        return resultado;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc=" Gestión de Respuestas ">
    /**
     * Crea la respuesta en formato html del listado de máquinas vending
     * registradas
     *
     * @return la respuesta creada
     */
    private String listarMaquinas() {
        String listado;

        listado = "<h2>Listado de máquinas vending:</h2>\n";

        for (int i = 0; i < maquinas.length; i++) {
            listado += "<a href='/vendingSD/maquina=" + i + "'>Máquina vending " + maquinas[i][1] + "</a>\n";
            listado += "<p><font color='red'>" + maquinas[i][7] + "</font></p>\n";
        }

        return listado;
    }

    /**
     * Crea la respuesta en formato html con los datos de una máquina vending en
     * concreto con la posibilidad de modificarlos
     *
     * @param p_id máquina a mostrar
     * @return respuesta creada
     */
    private String mostrarMaquina(int p_id) {
        String html = "";

        // Título
        html += "<div style='clear: both'>\n";
        html += "<h2>Máquina vending " + maquinas[p_id][1] + "</h2>\n";
        html += "<div style='align: top'><h6>Por favor, actualice los valores con cada modificación.</h6></div>\n";
        html += "<div><h6>Fecha: " + maquinas[p_id][2] + "</h6></div>\n";
        html += "</div>\n";

        html += "<div id=\"div_interruptor\" style='clear: both'>\n";
        html += "<p> Máquina ";

        if ((maquinas[p_id][6]).equals("false")) {
            html += "apagada.</p>\n";
            html += "<button onclick=\"location.href='/vendingSD/interruptor?maquina=" + p_id + "'\">Encender</button>\n";
        } else {
            html += "encendida.</p>\n";
            html += "<button onclick=\"location.href='/vendingSD/interruptor?maquina=" + p_id + "'\">Apagar</button>\n";
        }
        html += "</div>\n";

        html += "<div id=\"div_valores\" style='clear: both'>\n";

        // Stock
        html += "<div id=\"div_stock\">\n";
        html += "<p>Stock actual: " + maquinas[p_id][3] + "</p>\n";

        html += "<input type=\"text\" id=\"stock\" name=\"stock\" value=\"\"/>\n";
        html += "<button onclick=\"modificarStock(" + p_id + ")\">Modificar Stock</button>\n";
        html += "</div>\n";

        // Monedero
        html += "<div id=\"div_money\">\n";
        html += "<p>Monedero actual: " + maquinas[p_id][4] + "€</p>\n";
        html += "<input type=\"text\" id=\"money\" name=\"money\" value=\"\"/>\n";
        html += "<button onclick=\"modificarMoney(" + p_id + ")\">Modificar Monedero</button>\n";
        html += "</div>\n";

        // Temperatura
        html += "<div id=\"div_temp\">\n";
        html += "<p>Temperatura actual: " + maquinas[p_id][5] + "º</p>\n";
        html += "<input type=\"text\" id=\"temp\" name=\"temp\" value=\"\"/>\n";
        html += "<button onclick=\"modificarTemp(" + p_id + ")\">Modificar Temperatura</button>\n";
        html += "</div>\n";

        // Display
        html += "<div><p><font color='red'>" + maquinas[p_id][7] + "</font></p></div>\n";

        // Menús
        html += "</div><div>\n";
        html += "<button onclick=\"location.href='/vendingSD/maquinas'\">Volver al menú</button>\n";
        html += "<button onclick=\"location.href='/vendingSD/maquina=" + p_id + "'\">Actualizar valores</button>\n";
        html += "</div>\n";

        // Script
        html += "<script type=\"text/javascript\">"
                + "function modificarStock(id){\n"
                + "    var stock = document.getElementById(\"stock\").value;\n"
                + "    location.href=\"/vendingSD/gestionarStock?maquina=\" + id + \"&stock=\" + stock;\n"
                + "}\n"
                + "function modificarMoney(id){\n"
                + "    var money = document.getElementById(\"money\").value;\n"
                + "    location.href=\"/vendingSD/gestionarMonedero?maquina=\" + id + \"&monedero=\" + money;\n"
                + "}\n"
                + "function modificarTemp(id){\n"
                + "    var temp = document.getElementById(\"temp\").value;\n"
                + "    location.href=\"/vendingSD/gestionarTemperatura?maquina=\" + id + \"&temperatura=\" + temp;\n"
                + "}\n"
                + "</script>";

        return html;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc=" Gestión de  Peticiones "> 
    /**
     * Método auxiliar para la realización del parseo de peticiones
     *
     * @param p_param la variable y el valor con el formato nombre=valor
     * @return el valor de la variable
     */
    private int getValorInt(String p_param) {
        int valor = 0;
        String[] param = p_param.split("\\=");

        if (param.length == 2) {
            valor = Integer.parseInt(param[1]);
        } else {
            valor = -1;
        }

        return valor;
    }

    /**
     * Método auxiliar para la realización del parseo de peticiones
     *
     * @param p_param la variable y el valor con el formato nombre=valor
     * @return el nombre de la variable
     */
    private String getParam(String p_param) {
        String[] param = p_param.split("\\=");
        return param[0];
    }

    /**
     * Gestiona las peticiones que modifican valores de los objetos remotos
     * delegando las modificaciones a los correspondientes métodos
     *
     * @param p_op la operación a realizar: gestionarStock, gestionarMonedero o
     * gestionarTemperatura
     * @param p_params los parámetros de la petición, el id de la máquinav
     * vending o el valor del elemento a modificar
     * @return el cuerpo de la respuesta
     * @throws NumberFormatException
     */
    private String gestionarValores(String p_op, String p_params) throws NumberFormatException {
        String resultado = "";
        String parametro;
        String[] parametros;
        int id, valor;

        // formato: maquina=id || stock/monedero=value
        parametros = p_params.split("&");

        parametro = getParam(parametros[0]);
        if (parametro.equals("maquina")) {
            id = getValorInt(parametros[0]);
            parametro = getParam(parametros[1]);
            valor = getValorInt(parametros[1]);

            if (p_op.equals("gestionarStock") && parametro.equals("stock")) {
                resultado += gestionarStock(id, valor);

            } else if (p_op.equals("gestionarMonedero") && parametro.equals("monedero")) {
                resultado += gestionarMonedero(id, valor);
            } else if (p_op.equals("gestionarTemperatura") && parametro.equals("temperatura")) {
                resultado += gestionarTemperatura(id, valor);
            } else {
                resultado = "405";
            }
        } else {
            resultado = "405";
        }

        return resultado;
    }

    /**
     * Gestiona la petición recibida delegando la creación de la respuesta a los
     * métodos adecuados
     *
     * @param p_peticion la peticón completa recibida por sockets
     * @return el cuerpo de la respuesta
     */
    private String gestionarPeticion(String p_peticion) {
        String resultado = "405"; // el método no existe

        // formato: GET || resto
        String[] peticion = p_peticion.split(" ");

        if (peticion[0].equals("GET")) {
            // formato: _ || vendingSD || operación...
            String[] pet_SD = peticion[1].split("/");

            if (pet_SD.length < 2 || !pet_SD[1].equals("vendingSD")) {
                resultado = "404";
            } else {

                leerMaquinas();
                try {
                    // Comprobar si tenía Security Manager
                    System.setSecurityManager(new RMISecurityManager());

                    if (pet_SD[2].equals("maquinas")) {
                        resultado = listarMaquinas();

                    } else if (pet_SD[2].startsWith("maquina=")) {
                        int id = getValorInt(pet_SD[2]);
                        resultado = mostrarMaquina(id);

                    } else {
                        // formato: operación || parámetros
                        String[] pet_op = pet_SD[2].split("\\?");

                        if (pet_op[0].equals("interruptor")) {
                            resultado = interruptor(pet_op[1]);

                        } else if (pet_op[0].startsWith("gestionar")) {
                            resultado = gestionarValores(pet_op[0], pet_op[1]);

                        } // f_operaciones
                    } // f_maquinas o...
                } catch (Exception e) {
                    System.out.println("Error HiloMiniHTTP (gestionarPeticion): " + e.getMessage());
                    e.printStackTrace();
                }
            } // f_vendingSD
        }// f_GET

        return resultado;
    }
    //</editor-fold>

    /**
     * Extrae los datos de los objetos remotos almacenados en el fichero
     * "maquinas.txt"
     */
    private void leerMaquinas() {
        int total = 0, m = 0, d = 0;
        String line;

        try {
            // Abrir el fichero
            File file = new File(fichero);
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            // Leer total de máquinas
            if ((line = bufferedReader.readLine()) != null) {
                total = Integer.parseInt(line);

                maquinas = new String[total][total_datos];

                while ((line = bufferedReader.readLine()) != null) {
                    if (!line.equals("###")) {
                        maquinas[m][d] = line;
                        d++;
                    } else {
                        m++;
                        d = 0;
                    }
                }
            }

            fileReader.close();

        } catch (Exception e) {
            if (!e.getMessage().equals("Terminamos de leer")) {
                System.out.println("Error HiloMiniHTTP (leerMaquinas): " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /**
     * Método run que crea la cabecera de la respuesta y gestiona los posibles
     * errores en la gestión de la petición
     */
    @Override
    public void run() {
        String html = "";
        String peticion, resultado;
        String cabecera = "Connection: close\n"
                + "Content-Length: 2000\n"
                + "Content-Type: text/html; charset=\"UTF-8\"\n"
                + "Server: servidorMiniHTTP\n\n"
                + "<html>\n"
                + "<body>\n";
        try {
            System.out.println("<-->\nNuevo HiloMiniHTTP");
            peticion = leerSocket(skCliente);

            System.out.println("La peticion es: " + peticion);

            resultado = gestionarPeticion(peticion);

            switch (resultado) {
                case "404":
                    html = "HTTP/1.1 404 Not found\n";
                    html += cabecera;
                    html += "<h1>Error 404. Not found</h1>";
                    break;
                case "405":
                    html = "HTTP/1.1 405 Method not allowed\n";
                    html += cabecera;
                    html += "<h1>Error 405. Method not allowed</h1>";
                    break;
                case "500":
                    html = "HTTP/1.1 500 Internal Server Error\n";
                    html += cabecera;
                    html += "<h1>Error 500. Internal Server Error</h1>";
                    break;
                default:
                    html = "HTTP/1.1 200 OK\n";
                    html += cabecera;
                    html += resultado;
                    break;
            }

            html += "</body>\n</html>";
            escribirSocket(skCliente, html);

            System.out.println("Fin HiloMiniHTTP\n<--->\n");
            skCliente.close();

        } catch (Exception e) {
            System.out.println("Error HiloMiniHTTP (run): " + e.getMessage());
            e.printStackTrace();
        }
    }

}
