import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * HiloControlador: clase con la que el controlador hace consultas cada dos
 * segundos al servidor RMI y almacena los valores de los objetos en un
 * fmaquinas (maquinas.txt) que las peticiones consultan.
 *
 * @author Carolina Prada Hernández
 */
public class HiloControlador extends Thread {

    /**
     * Nombre del fichero donde almacenar los datos de los objetos remotos
     */
    private final String fmaquinas;

    /**
     * Lista con las direcciones de los servidores RMI a los que consultar
     */
    private final List<String> servidores;

    /**
     * Lista con los objetos remotos y sus datos
     */
    private List<List<String>> maquinas;

    /**
     * Constructor de la clase
     *
     * @param p_fichero
     */
    HiloControlador(String p_fichero) {
        fmaquinas = p_fichero;
        servidores = new ArrayList<String>();
        maquinas = new ArrayList<List<String>>();
    }

    /**
     * Método que lee el fichero con las direcciones de los servidores RMI para
     * almacenarlas en la lista servidores
     */
    private void leerServidoresRMI() {
        try {
            // Abrir el fichero
            File file = new File("servidores.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";

            // Leer total de máquinas
            System.out.println("Servidores: ");
            while ((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                servidores.add(line);
            }

            fileReader.close();

        } catch (Exception e) {
            if (!e.getMessage().equals("Terminamos de leer")) {
                System.out.println("Error HiloMiniHTTP (leerMaquinas): " + e.getMessage());
                e.printStackTrace();
                System.exit(MIN_PRIORITY);
            }
        }
    }

    /**
     * Obtiene las máquinas vending registradas en los servidores, consulta sus
     * datos y los almacena en la lista maquinas
     */
    private void consultar() {
        InterfazMaquina maquina = null;
        String lectura;
        List<String> nombres = new ArrayList<String>();
        String[] aux;

        maquinas = new ArrayList<List<String>>();
        try {
            // Obtener los nombres
            System.setSecurityManager(new RMISecurityManager());
            for (String servidor : servidores) {
                aux = Naming.list(servidor);
                if (aux.length > 0) {
                    nombres.addAll(Arrays.asList(aux));
                }
            }

            System.out.println("Listado de nombres (" + nombres.size() + "):");
            for (String url : nombres) {
                // Leer máquina
                maquina = (InterfazMaquina) Naming.lookup(url);
                lectura = maquina.leer();
                System.out.println("[" + url + "] " + lectura);

                aux = new String[8];

                // Guardar datos
                String[] valores = lectura.split("&&");
                aux[0] = url;
                int i = 1;
                for (String dato : valores) {
                    aux[i] = dato;
                    i++;
                }

                maquinas.add(Arrays.asList(aux));
            }

        } catch (Exception e) {
            System.out.println("Error HiloConsultor (consultar): " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Comprueba algunos valores (stock, monedero y temperatura) para gestionar
     * automáticamente las máquinas vending en caso de error en esos valores
     */
    private void comprobar() {
        InterfazMaquina maquina;
        int valor;
        String display;

        System.setSecurityManager(new RMISecurityManager());

        try {

            for (List<String> m : maquinas) {
                maquina = (InterfazMaquina) Naming.lookup(m.get(0));
                display = "";

                // Check Stock
                valor = Integer.parseInt(m.get(3));
                if (valor == -1) {
                    display += "Máquina atascada.";
                } else if (valor <= 10) {
                    m.set(3, "-1");
                    maquina.setStock(-1);
                    display += "Máquina sin stock.";
                }

                // Check Monedero
                valor = Integer.parseInt(m.get(4));
                if (valor > 800) {
                    m.set(4, "-1");
                    maquina.setMonedero(-1);
                    display += "Máquina sin cambio.";
                } else if (valor == -1) {
                    display += "Máquina sin cambio.";
                }

                // Check Temperatura
                valor = Integer.parseInt(m.get(5));
                if (valor > 10) {
                    System.out.print("Mayor que 10: " + valor);
                    valor = valor - 2;
                    System.out.println(" ->> " + valor);
                    m.set(5, valor + "");
                    maquina.setTemperatura(valor);
                    display += "Máquina sobrecalentada.";
                }

                // Actualizando Estado
                if (m.get(6).equals("true") && !display.equals("")) {
                    m.set(6, "false");
                    maquina.setEstado(false);
                }

                // Actualizando Display
                m.set(7, display);
                maquina.setDisplay(display);

            }
        } catch (Exception e) {
            System.out.println("Error HiloConsultor (comprobar): " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
     * Almacena los datos ya comprobados y actualizados en un fichero de texto
     * para que las peticiones los consulten de forma más eficiente
     */
    private void escribirMaquinas() {
        try {
            // Crear el el fichero fmaquinas
            FileWriter fstream = new FileWriter(fmaquinas);
            BufferedWriter out = new BufferedWriter(fstream);

            out.write(maquinas.size() + "\n");

            for (List<String> maquina : maquinas) {
                for (String dato : maquina) {
                    out.write(dato + "\n");
                }
                out.write("###\n");
            }

            // Cerrar el fmaquinas
            out.close();
        } catch (Exception e) {
            System.out.println("Error HiloConsultor (consultar): " + e.getMessage());
            e.printStackTrace();
            System.exit(MIN_PRIORITY);
        }
    }

    /**
     * Método run del hilo que actualiza los datos cada 2 segundos
     */
    @Override
    public void run() {
        leerServidoresRMI();

        try {
            for (;;) {
                consultar();
                comprobar();
                escribirMaquinas();
                sleep(2000);
            }
        } catch (Exception e) {//Catch exception if any
            System.out.println("Error HiloConsultor (run): " + e.getMessage());
            e.printStackTrace();
        }
    }

}
