import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * ObjetocMaquina: objeto remoto para la comunicación RMI.
 *
 * @author Carolina Prada Hernández
 */
public class ObjetoMaquina extends UnicastRemoteObject
        implements InterfazMaquina, Serializable {

    /**
     * Nombre identificativo de la máquina
     */
    private String nombre;

    /**
     * Valor del stock de la máquina -1 significa que la máquina está atascada
     */
    private int stock;

    /**
     * Valor del monedero de la máquina -1 si no tiene cambio
     */
    private int monedero;

    /**
     * Valor de la temperatura de la máquina
     */
    private int temperatura;

    /**
     * Estado de la máquina -1: apagada 0: encendida
     */
    private int estado;

    /**
     * Display de la máquina
     */
    private String display;

    /**
     * Constructor por defecto
     *
     * @throws java.rmi.RemoteException
     */
    public ObjetoMaquina() throws java.rmi.RemoteException {
        super();
        estado = -1;
    }

    /**
     * Constructor que inicializa los valores de la máquina de forma aleatoria
     * dentro de los valores admitidos
     *
     * @param p_nombre
     * @throws java.rmi.RemoteException
     */
    public ObjetoMaquina(String p_nombre) throws java.rmi.RemoteException {
        nombre = p_nombre;
        stock = (int) (Math.random() * 60 + 0);
        monedero = (int) (Math.random() * 1000 + 0);
        temperatura = (int) (Math.random() * 50 + 0);
        estado = -1;
    }

    /**
     * Crea una lectura con los datos de la máquina
     *
     * @return lectura con los valores separados por los carácteres "&&"
     * @throws RemoteException
     */
    @Override
    public String leer() throws RemoteException {
        String result = "";
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        result += getNombre();
        result += "&&" + (dateFormat.format(date));
        result += "&&" + getStock();
        result += "&&" + getMonedero();
        result += "&&" + getTemperatura();
        result += "&&" + getEstado();
        result += "&&" + getDisplay();

        System.out.println("Leyendo sensor: " + result);
        return result;
    }

    //<editor-fold defaultstate="collapsed" desc=" Setters y Getters de la clase ">
    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @return the stock
     */
    public int getStock() {
        return stock;
    }

    /**
     * @param p_stock the stock to set
     * @throws java.rmi.RemoteException
     */
    @Override
    public void setStock(int p_stock) throws RemoteException {
        System.out.println("Modificando stock: " + stock + " --> " + p_stock);
        stock = p_stock;
    }

    /**
     * @return the monedero
     */
    public int getMonedero() {
        return monedero;
    }

    /**
     * @param p_monedero the monedero to set
     * @throws java.rmi.RemoteException
     */
    @Override
    public void setMonedero(int p_monedero) throws RemoteException {
        System.out.println("Modificando monedero: " + monedero + " --> " + p_monedero);
        monedero = p_monedero;
    }

    /**
     * @return the temperatura
     */
    public int getTemperatura() {
        return temperatura;
    }

    /**
     * @param p_temperatura the temperatura to set
     */
    @Override
    public void setTemperatura(int p_temperatura) throws RemoteException {
        System.out.println("Modificando temperatura: " + temperatura + " --> " + p_temperatura);
        temperatura = p_temperatura;
    }

    /**
     * @return the estado
     */
    public Boolean getEstado() {
        return estado == 0;
    }

    /**
     * @param p_estado the estado to set
     * @throws java.rmi.RemoteException
     */
    @Override
    public void setEstado(Boolean p_estado) throws RemoteException {
        System.out.print("Modificando estado: " + estado + " --> ");
        if (p_estado) {
            estado = 0;
        } else {
            estado = -1;
        }
        System.out.println(estado);
    }

    /**
     * @return the display
     */
    public String getDisplay() {
        return display;
    }

    /**
     * @param p_display
     */
    @Override
    public void setDisplay(String p_display) {
        System.out.println("Modificando display:\n" + display + " -->\n" + p_display);
        display = p_display;
    }
    //</editor-fold>

}
