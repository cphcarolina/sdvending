import java.rmi.Remote;

/**
 * InterfazMaquina: interfaz remota de los objetos máquina para la comunicación
 * RMI.
 *
 * @author Carolina Prada Hernández
 */
public interface InterfazMaquina extends Remote {

    /**
     * Devuelve los datos del sensor
     *
     * @return la lectura de los datos
     * @throws java.rmi.RemoteException
     */
    public String leer() throws java.rmi.RemoteException;

    /**
     * Modifica la temperatura
     *
     * @param p_valor el nuevo valor
     * @throws java.rmi.RemoteException
     */
    public void setTemperatura(int p_valor) throws java.rmi.RemoteException;

    /**
     * Modifica el valor del stock
     *
     * @param p_stock el nuevo valor
     * @throws java.rmi.RemoteException
     */
    public void setStock(int p_stock) throws java.rmi.RemoteException;

    /**
     * Modifica el valor del monedero
     *
     * @param p_monedero el nuevo valor
     * @throws java.rmi.RemoteException
     */
    public void setMonedero(int p_monedero) throws java.rmi.RemoteException;

    /**
     * Enciende o apaga la máquina
     *
     * @param p_estado el nuevo valor
     * @throws java.rmi.RemoteException
     */
    public void setEstado(Boolean p_estado) throws java.rmi.RemoteException;

    /**
     * Modifica el display
     *
     * @param p_display el nuevo menasaje
     * @throws java.rmi.RemoteException
     */
    public void setDisplay(String p_display) throws java.rmi.RemoteException;

}
