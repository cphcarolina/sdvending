import java.rmi.Naming;
import java.rmi.RMISecurityManager;

/**
 * Registro: clase que se encarga del registro de un objeto remoto de tipo
 * ObjetoMaquina y de gestión local a través de una interfaz de escritorio.
 *
 * @author Carolina Prada Hernández
 */
public class Registro {

    /**
     * Lanza la interfaz gráfica para que se modifiquen los datos de la máquina
     * vending
     */
    private static void lanzarUI() {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UIMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UIMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UIMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UIMaquina.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new UIMaquina(maquina).setVisible(true);
            }
        });
    }

    /**
     * Se encarga de crear una máquina vending, de registrarla en el servidor
     * RMI y lanza la interfazgráfica
     *
     * @param args
     */
    public static void main(String args[]) {
        String nombre;

        if (args.length == 1) {
            try {
                // Antes hacía la comprobación if(System.getSecurityManager==null)...
                if (System.getSecurityManager() == null) {
                    System.setSecurityManager(new RMISecurityManager());
                }
                nombre = "maquina_" + args[0];
                maquina = new ObjetoMaquina(nombre);
                String urlRegistro = "//localhost:1099/" + nombre;

                Naming.rebind(urlRegistro, maquina);
                System.out.println("Maquina " + args[0] + " registrada (" + urlRegistro + ")");

                lanzarUI();
            } catch (Exception e) {
                System.out.println("Error Registro: " + e.getMessage());
                e.printStackTrace();
                System.exit(-1);
            }
        } else {
            System.out.println("Error Registro: No se ha registrado ningún objeto");
        }
    }

    /**
     * Máquina vending a registrar
     */
    private static ObjetoMaquina maquina;
}
