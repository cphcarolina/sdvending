import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;

/**
 * UIMaquina: Interfaz gráfica para gestionar localmente los objetos remotos de
 * tipo ObjetoMaquina.
 *
 * @author Carolina Prada Hernández
 */
public class UIMaquina extends javax.swing.JFrame {

    /**
     * Creates new form UIMaquina
     *
     * @param p_maquina
     */
    public UIMaquina(ObjetoMaquina p_maquina) {
        maquina = p_maquina;
        initComponents();

        labelMaquina.setText("Máquina: " + maquina.getNombre());

        textStock.setText(String.valueOf(maquina.getStock()));
        textMoney.setText(String.valueOf(maquina.getMonedero()));
        textTemp.setText(String.valueOf(maquina.getTemperatura()));
        labelDisplay.setText(maquina.getDisplay());

        labelStock.setText("");
        labelMoney.setText("");
        labelTemp.setText("");
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelMaquina = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        textStock = new javax.swing.JTextField();
        labelStock = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        textMoney = new javax.swing.JTextField();
        labelMoney = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        textTemp = new javax.swing.JTextField();
        labelTemp = new javax.swing.JLabel();
        labelDisplay = new javax.swing.JLabel();
        buttonActualizar = new javax.swing.JButton();
        buttonSalir = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        checkOn = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        labelMaquina.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelMaquina.setText("jLabel1");

        jLabel1.setText("Stock");

        textStock.setText("jTextField1");

        labelStock.setText("jLabel2");

        jLabel2.setText("Monedero");

        textMoney.setText("jTextField1");

        labelMoney.setText("jLabel3");

        jLabel3.setText("Temperatura");

        textTemp.setText("jTextField1");

        labelTemp.setText("jLabel4");

        labelDisplay.setText("jLabel4");

        buttonActualizar.setText("Actualizar");
        buttonActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonActualizarActionPerformed(evt);
            }
        });

        buttonSalir.setText("Salir");
        buttonSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonSalirActionPerformed(evt);
            }
        });

        jLabel4.setText("Estado");

        checkOn.setText("Encendido");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(labelMaquina))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(labelDisplay))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(24, 24, 24)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(jLabel3)
                                                .addGap(18, 18, 18)
                                                .addComponent(textTemp, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE))
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel1)
                                                    .addComponent(jLabel2))
                                                .addGap(36, 36, 36)
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                                    .addComponent(textStock, javax.swing.GroupLayout.DEFAULT_SIZE, 70, Short.MAX_VALUE)
                                                    .addComponent(textMoney, javax.swing.GroupLayout.PREFERRED_SIZE, 1, Short.MAX_VALUE))))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(8, 8, 8)
                                                .addComponent(labelMoney))
                                            .addGroup(layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(labelTemp))
                                            .addGroup(layout.createSequentialGroup()
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(labelStock))))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel4)
                                        .addGap(55, 55, 55)
                                        .addComponent(checkOn)))))
                        .addGap(0, 232, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(buttonActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(buttonSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(labelMaquina)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelStock, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(textStock, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(textMoney, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelMoney))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(textTemp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelTemp))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(checkOn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addComponent(labelDisplay)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(buttonActualizar)
                    .addComponent(buttonSalir))
                .addContainerGap())
        );

        labelDisplay.getAccessibleContext().setAccessibleName("labelDisplay");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Quita el registro de la máquina vending en el servidor RMI local al
     * cerrar la aplicación
     *
     * @param evt
     */
    private void buttonSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonSalirActionPerformed
        try {
            if (System.getSecurityManager() == null) {
                System.setSecurityManager(new RMISecurityManager());
            }
            String url = "//localhost:1099/" + maquina.getNombre();
            Naming.unbind(url);
            System.exit(0);

        } catch (Exception e) {
            System.out.println("Error UIMaquina: " + e.getMessage());
            e.printStackTrace();
        }
    }//GEN-LAST:event_buttonSalirActionPerformed

    /**
     * Cambia los valores de la máquina vending por los introducidos en la
     * interfaz gráfica a no ser de que éstos estén fuera de rango
     *
     * @param evt
     */
    private void buttonActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_buttonActualizarActionPerformed
        Boolean actualizar = checkValores();

        if (actualizar) {
            try {
                maquina.setStock(Integer.parseInt(textStock.getText()));
                maquina.setMonedero(Integer.parseInt(textMoney.getText()));
                maquina.setTemperatura(Integer.parseInt(textTemp.getText()));
                maquina.setEstado(checkOn.isSelected());
                labelDisplay.setText(maquina.getDisplay());
            } catch (RemoteException e) {
                System.out.println("Error UIMaquina: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_buttonActualizarActionPerformed

    /**
     * Comprueba los valores introducidos para mostrar mensajes de alerta o
     * error al usuario. En caso de errores en los valores, la máquina no se
     * actualizará.
     *
     * @return si se puede actualizar o no.
     */
    private Boolean checkValores() {
        Boolean ok = true;          // true si los valores límite se cumplen sino no se actualiza

        labelStock.setText("");
        labelMoney.setText("");
        labelTemp.setText("");
        labelDisplay.setText("");

        int stock = Integer.parseInt(textStock.getText());
        int money = Integer.parseInt(textMoney.getText());
        int temp = Integer.parseInt(textTemp.getText());

        if (stock < -1 || stock > 60) {
            labelStock.setText("Valor no válido para stock [-1,60].");
            ok = false;
        }
        if (money < -1 || money > 1000) {
            labelMoney.setText("Valor no válido para monedero [-1,1000].");
            ok = false;
        }
        if (temp < 0 || temp > 50) {
            labelTemp.setText("Valor no válido para temperatura [0,50].");
            ok = false;
        }

        Boolean encender = checkOn.isSelected();
        checkOn.setSelected(encender);

        return ok;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton buttonActualizar;
    private javax.swing.JButton buttonSalir;
    private javax.swing.JCheckBox checkOn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel labelDisplay;
    private javax.swing.JLabel labelMaquina;
    private javax.swing.JLabel labelMoney;
    private javax.swing.JLabel labelStock;
    private javax.swing.JLabel labelTemp;
    private javax.swing.JTextField textMoney;
    private javax.swing.JTextField textStock;
    private javax.swing.JTextField textTemp;
    // End of variables declaration//GEN-END:variables

    /**
     * Máquina vending asociada a la interfaz gráfica
     */
    private final ObjetoMaquina maquina;
}
